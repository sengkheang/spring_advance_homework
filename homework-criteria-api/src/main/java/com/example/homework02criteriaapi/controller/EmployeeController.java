package com.example.homework02criteriaapi.controller;

import com.example.homework02criteriaapi.entities.Employee;
import com.example.homework02criteriaapi.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/employee")
public class EmployeeController {


    private EmployeeRepo employeeRepo;

    public EmployeeController(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

//    1.
    @RequestMapping(path = "/all", produces = {"application/json"})
    public List<Employee> getAllEmployee() {
        return employeeRepo.getAllEmployee();
    }

//    2.
    @RequestMapping(path = "/allByDepartment/{id}")
    public List<Employee> getEmployeeByDepartment(@PathVariable Integer id) {
        return employeeRepo.getEmployeeByDepartmentId(id);
    }

//    3.
    @RequestMapping(path = "/{id}")
    public List<Employee> getEmployeeById(@PathVariable Integer id) {
        return employeeRepo.getEmployeeById(id);
    }

//    4.
    @RequestMapping(path = "/allSalary")
    public List sumEmployeeSalary() {


        List<Tuple> tuples = employeeRepo.sumAllSalary();
        List<Object> employees = new ArrayList<>();
        for (Tuple t: tuples
             ) {
            employees.add("Salary"+t.get(0));
        }
        return employees;
    }

//    5.
    @RequestMapping(path = "/wrapIntoOneObject")
    public List<Object> wrapEmployeeIntoOneObject(){
       List<Tuple> tuples= employeeRepo.wrapEmployeeIntoObject();
       List<Object> objects=new ArrayList<>();
       for (Tuple tuple: tuples){
        objects.add(tuple.get(0));
           objects.add(tuple.get(1));
           objects.add(tuple.get(2));
           objects.add(tuple.get(3));
       }
        return objects;
    }

//    6.
    @RequestMapping(path = "/getAllEmployeeByDepartment",method = RequestMethod.GET)
    public List<Object> getAllSpecificDep(){
        List<Tuple> tuples= employeeRepo.selectAllDepartment();
        List<Object> objects=new ArrayList<>();
        for (Tuple tuple:
                tuples
             ) {
            objects.add("Department_Id: " + tuple.get(0) +
                    "  ,Department_Name: " + tuple.get(1) + "  ,Total_Employee: " + tuple.get(2));

        }
        return objects;
    }

//    7.
    @RequestMapping(path = "/maxSalary")
    public Double getMaxSalary(){
        return employeeRepo.getMaxSalary();
    }

//    8.
    @RequestMapping(path = "/SalaryBetween")
    public List<Employee> getSalaryBetween(
            @RequestParam(defaultValue = "500.0", value = "max", required = false) Double max,
            @RequestParam(defaultValue = "300.0", value = "min", required = false) Double min
    ){
        return employeeRepo.getSalaryBetween(min, max);
    }

//    9.
    @RequestMapping(path = "/setSalary/department/{id}")
    public String setEmployeeSalaryByDepartmentId(
            @PathVariable Integer id
    ){
        employeeRepo.setEmployeeSalaryByDepartment(id);
        return "Update Employee's salary successfully!!!";
    }

//    10.
    @RequestMapping(path = "/updateInfo/{id}")
    public String updateEmployeeInfo(@PathVariable Integer id){
        employeeRepo.updateEmployeeInfoById(id);
        return "Update Employee's info successfully!!!";
    }

    @RequestMapping(path = "/delete/{id}")
    public String deleteEmployeeById(@PathVariable Integer id){
        employeeRepo.deleteEmployeeById(id);
        return "Delete Employee successfully!!!";
    }
}
