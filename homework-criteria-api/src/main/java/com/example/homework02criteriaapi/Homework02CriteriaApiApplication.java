package com.example.homework02criteriaapi;

import com.example.homework02criteriaapi.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;


@SpringBootApplication
@Transactional
public class Homework02CriteriaApiApplication implements ApplicationRunner {


    @Autowired
    private EmployeeRepo employeeRepo;

    public static void main(String[] args) {
        SpringApplication.run(Homework02CriteriaApiApplication.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {


        System.out.println(employeeRepo.getAllEmployee());
        System.out.println(employeeRepo.getEmployeeByDepartmentId(2));
        System.out.println(employeeRepo.getEmployeeById(3));
        System.out.println(employeeRepo.wrapEmployeeIntoObject());
        System.out.println(employeeRepo.getSalaryBetween(300.0, 500.0));
//        System.out.println(employeeRepo.setEmployeeSalaryByDepartment(2));
        System.out.println(employeeRepo.updateEmployeeInfoById(3));
    }
}
