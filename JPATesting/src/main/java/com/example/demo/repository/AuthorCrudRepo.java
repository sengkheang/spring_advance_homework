package com.example.demo.repository;

import com.example.demo.OnetoMany.Author;
import org.springframework.data.repository.CrudRepository;
public interface AuthorCrudRepo extends CrudRepository<Author, Integer > {

}
