package com.example.demo.repository;

import com.example.demo.OnetoOne.Student;
import org.springframework.data.repository.CrudRepository;


public interface StudentCrudRepo extends CrudRepository<Student, Integer> {
}
