package com.example.demo.repository;

import com.example.demo.ManyToMany.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductCrudRepo extends CrudRepository<Product, Integer> {
}
