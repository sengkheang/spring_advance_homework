package com.example.demo.ManyToMany;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tblProduct")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "product_name")
    private String ProductName;

    @ManyToMany(targetEntity = Supplier.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Supplier> suppliers;

    public Product(String productName, List<Supplier> suppliers) {
        ProductName = productName;
        this.suppliers = suppliers;
    }

    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", ProductName='" + ProductName + '\'' +
                '}';
    }
}
