package com.example.demo.OnetoOne;


import javax.persistence.*;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "[home_address]")
    private String home_address;

    @Column(name = "[city_name]")
    private String city_name;

    @Column(name = "[country_name]")
    private String country_name;

    public Address(String home_address, String city_name, String country_name) {
        this.home_address = home_address;
        this.city_name = city_name;
        this.country_name = country_name;
    }

    public Address() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHome_address() {
        return home_address;
    }

    public void setHome_address(String home_address) {
        this.home_address = home_address;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }
}
