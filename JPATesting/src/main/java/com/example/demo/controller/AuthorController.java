package com.example.demo.controller;

import com.example.demo.OnetoMany.Author;
import com.example.demo.OnetoMany.Book;
import com.example.demo.repository.AuthorCrudRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class AuthorController{

    @Autowired
    private AuthorCrudRepo authorCrudRepo;

    @GetMapping("/addAuthor")
    @ResponseBody
    public String addAuthor(){
        Author author=new Author();
        Book book=new Book();
        book.setBookName("khmer love khmer");
        author.setAuthorName("love");
        author.setBooks(Collections.singletonList(book));
        authorCrudRepo.save(author);
        return "Add Author Success!!";
    }

    @GetMapping("/updateAuthor/{id}")
    @ResponseBody
    public String updateAuthor(@PathVariable Integer id){
        Author author =new Author();
        author=authorCrudRepo.findById(id).get();
        author.setAuthorName("Stephene Lawrance");

        authorCrudRepo.save(author);
        return "Update Author Successful";
    }

    @GetMapping("/findAuthorAll")
    @ResponseBody
    public String findAuthorAll(){
        return "" + authorCrudRepo.findAll();
    }

    @GetMapping("/findAuthor/{id}")
    @ResponseBody
    public String findAuthorById(@PathVariable Integer id){
        return "" + authorCrudRepo.findById(id).get();
    }

    @GetMapping("/deleteAuthor/{id}")
    @ResponseBody
    public String deleteAuthorById(@PathVariable Integer id){

        Author author=new Author();
        author=authorCrudRepo.findById(id).get();

        authorCrudRepo.delete(author);

        return "Delete Author Successful!!!";
    }
}
