package com.example.demo.controller;

import com.example.demo.OnetoOne.Address;
import com.example.demo.OnetoOne.Student;
import com.example.demo.repository.StudentCrudRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class StudentController {

    @Autowired
    private StudentCrudRepo studentCrudRepo;
    @RequestMapping("/addStudent")
    @ResponseBody
    public String add(){
        Student student=new Student();
        Address address=new Address();
        address.setHome_address("E116");
        address.setCity_name("PP");
        address.setCountry_name("CA");

        student.setStudentName("Kheang");
        student.setAddress(address);
        studentCrudRepo.save(student);
        return "Add Student Success!";

    }


    @RequestMapping("/updateStudent/{id}")
    @ResponseBody
    public String update(@PathVariable Integer id){
        Student student=new Student();
         student=studentCrudRepo.findById(id).get();
        student.setStudentName("Pheatra");

        studentCrudRepo.save(student);
        return "Update Student Success!!";
    }

    @RequestMapping("/findStudentAll")
    @ResponseBody
    public String find(){

        return ""+studentCrudRepo.findAll();
    }

    @RequestMapping("/findStudent/{id}")
    @ResponseBody
    public String findById(@PathVariable Integer id){
        return ""+studentCrudRepo.findById(id).get();
    }

    @RequestMapping("/deleteStudent/{id}")
    @ResponseBody
    public String delete(@PathVariable Integer id){
        Student student=new Student();
        student=studentCrudRepo.findById(id).get();

        studentCrudRepo.delete(student);
        return "Delete Student Success!!!";
    }
}
